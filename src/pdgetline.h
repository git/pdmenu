/*
 * Copyright (c) 1995, 1996, 1997 Joey Hess (pdmenu@joeyh.name)
 * All rights reserved. See COPYING for full copyright information (GPL).
 */

char* pdgetline (FILE *, int);
